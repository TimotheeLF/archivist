# Coding guidelines
- Keywords used by the compiler -> ALLCAPS
- Keywords used by me -> camelCase
- Always use explicit parameters -> WRITE (UNIT=...)
