PROGRAM parser
    CHARACTER(LEN=4) :: signature
    CHARACTER(LEN=2) :: versionNeeded, compressionMethod, fileNameLengthC, extraLengthC
    CHARACTER, ALLOCATABLE :: fileName(:)
    INTEGER :: fileNameLength, extraLength

    OPEN (UNIT=1, FILE='53111.zip', ACCESS='STREAM', FORM='UNFORMATTED', ACTION='READ', STATUS='OLD')
    READ (UNIT=1, POS=1) signature, versionNeeded
    READ (UNIT=1, POS=9) compressionMethod
    READ (UNIT=1, POS=27) fileNameLengthC, extraLengthC

    ! PK\003\004 -> ZIP Header
    IF (signature == 'PK' // ACHAR (003) // ACHAR (004)) THEN
        WRITE (UNIT=*, FMT='(A)') "Detected file format: ZIP"
    ELSE
        WRITE (UNIT=*, FMT='(A)') "No compatible format found :("
        STOP
    END IF

    fileNameLength = IACHAR (fileNameLengthC(1:1))
    extraLength = IACHAR (extraLengthC(1:1))

    ALLOCATE (fileName(fileNameLength))
    READ (UNIT=1, POS=31) fileName


    WRITE (UNIT=*, FMT='(A)', ADVANCE='NO') "Signature: "
    DO i = 1, 4
        WRITE (UNIT=*, FMT='(I3)', ADVANCE='NO') IACHAR (signature(i:i))
    END DO

    WRITE (UNIT=*, FMT='(/,A,2I2)') "Minimum version: ", IACHAR (versionNeeded(1:1)), IACHAR (versionNeeded(2:2))
    WRITE (UNIT=*, FMT='(A,2I2)') "Compression method: ", IACHAR (compressionMethod(1:1)), IACHAR (compressionMethod(2:2))
    WRITE (UNIT=*, FMT='(A,I2)') "File name length: ", fileNameLength
    WRITE (UNIT=*, FMT='(A,I2)') "Extra field length: ", extraLength

    WRITE (UNIT=*, FMT='(A)', ADVANCE='NO') "File name: "
    DO i = 1, fileNameLength
        WRITE (UNIT=*, FMT='(A)', ADVANCE='NO') fileName(i:i)
    END DO
    WRITE (UNIT=*, FMT=*)

    CLOSE (UNIT=1)
END PROGRAM
